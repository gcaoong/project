import sqlite3

# 连接到SQLite数据库
# 如果数据库不存在，则会自动创建
conn = sqlite3.connect('data_storage.db')

# 创建一个Cursor对象，用于执行SQL命令
cursor = conn.cursor()

# 创建data_records表
cursor.execute('''
CREATE TABLE IF NOT EXISTS data_records (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    integer_data_1 INTEGER,
    integer_data_2 INTEGER,
    integer_data_3 INTEGER,
    float_data_1 REAL,
    float_data_2 REAL,
    float_data_3 REAL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP
)
''')

print("Table created successfully.")


# 插入一条记录
cursor.execute('''
INSERT INTO data_records (integer_data_1, integer_data_2, integer_data_3, float_data_1, float_data_2, float_data_3)
VALUES (10, 20, 30, 1.1, 2.2, 3.3)
''')

# 提交事务
conn.commit()

print("Data inserted successfully.")


# 更新id为1的记录
cursor.execute('''
UPDATE data_records
SET integer_data_1 = 100, float_data_1 = 9.9
WHERE id = 1
''')

# 提交事务
conn.commit()

print("Data updated successfully.")

# 删除id为1的记录
cursor.execute('''
DELETE FROM data_records WHERE id = 1
''')

# 提交事务
conn.commit()

print("Data deleted successfully.")


# 查询所有记录
cursor.execute('''
SELECT * FROM data_records
''')
rows = cursor.fetchall()

for row in rows:
    print(row)

# 关闭Cursor和Connection
cursor.close()
conn.close()